import sys
import heapq
import Queue
import decimal

def main():
    printQueue = Queue.Queue()
    header = sys.stdin.readline().split()
    n = int(header[0])
    m = int(header[1])
    
    while((n > 0) and (m > 0)):
        corridors = {}
        for i in range(0, n):
            corridors[i] = {}
            
        for i in range(0, m):
            corridor = sys.stdin.readline().split()
            #print corridor
            x = int(corridor[0])        # from
            y = int(corridor[1])        # to
            f = float(corridor[2]) * -1 # weight
            if x == y:
                pass # ignore corridors to self
            elif y in corridors[x] and corridors[x][y] < f:
                pass
            else:
                corridors[y][x] = f
                corridors[x][y] = f
        endSize = round(abs(shorty(n, corridors)[n-1]),4)
        printQueue.put(endSize)
        header = sys.stdin.readline().split()
        n = int(header[0])
        m = int(header[1])   
        
    while printQueue.empty() is False:
        print format(printQueue.get(), '.4f')
        
    sys.exit()

def shorty(n, corridors):
    dist = [0] * n
    prev = [None] * n
    done = [False] * n

    dist[0] = 1
    heap = []
    heapq.heappush(heap,(-1, 0))

    while len(heap) > 0:
        #print heap
        popped = heapq.heappop(heap)
        u = popped[1]
        done[u] = True
        corridorDict = corridors[u]
        for v in corridorDict:
            w = corridorDict[v]
            #print v, dist[u]*-w
            if dist[v] < dist[u] * -w:
                dist[v] = float(dist[u] * -w)
                prev[v] = u
                if done[v] == False:
                    heapq.heappush(heap, (-dist[v], v))
    #print dist
    return dist

if __name__ == "__main__":
    main()
