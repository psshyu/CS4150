# CS4150
### Algorithms - Kattis Solutions
All are in Python

### Assignments
1. Mr. Anaga - sort
2. Ceiling Function - treeeeeees
3. Galaxy Quest - Moore's Voting Algorithm
4. Auto-sink - DFS
5. Rumor Mill - BFS
6. Get Shorty - Dijkstra's
7. Number Theory - gcd, modExp, modInverse, isPrime, etc.
8. Bank Queue - Optimization
9. Under the Rainbow - Optimization/Dynamic Programming
10. Narrow Art Gallery - Optimization/Dynamic Programming
