import sys

def GCD(a, b):
    while(b > 0):
        remainder = a % b;
        a = b;
        b = remainder;
    return a

def exp(x, y, N):
    z = 1
    for bit in bin(y)[2:]:
        if bit == '1':
            z = (x * z * z) % N
        else:
            z = (z*z) % N
    return z

def ee(a, b):
    if b == 0:
        return 1,0,a
    else:
        x, y, d = ee(b, a % b)
        return y, x-(a/b)*y, d

def inverse(a, N):
    x, y, d = ee(a, N)
    if d == 1:
        return  x % N
    else:
        return "none"

def isPrime(p):
    if exp(2, p-1, p) == 1 and exp(3, p-1, p) == 1 and exp(5, p-1, p) == 1:
        print "yes"
    else:
        print "no"

def key(p, q):
    print p*q, 
    phi = (p-1)*(q-1)
    publicExp = 2;
    while(publicExp < phi and GCD(publicExp, phi) != 1):
        publicExp += 1
    print publicExp, 
    print(inverse(publicExp, phi))

def main():
    lines = sys.stdin.readlines()
    for line in lines:
        splitted = line.split()
        if splitted[0] == "gcd":
            print(GCD(int(splitted[1]), int(splitted[2])))
        if splitted[0] == "exp":
            print(exp(int(splitted[1]), int(splitted[2]), int(splitted[3])))
        if splitted[0] == "inverse":
            print(inverse(int(splitted[1]), int(splitted[2])))
        if splitted[0] == "isprime":
            isPrime(int(splitted[1]))        
        if splitted[0] == "key":
            key(int(splitted[1]), int(splitted[2]))
            
if __name__ == "__main__":
    main()
