import sys

cache = {}
def maxValue(N, v, r, unclosed, k):
    theMax = 0
    if (r,unclosed, k) in cache:
        return cache[(r,unclosed, k)]

    if k < N - r and r < N:
        if unclosed == -1:
            zero = v[r][0]+maxValue(N,v,r+1,0,k-1)
            one = v[r][1]+maxValue(N,v,r+1,1,k-1)
            negOne = v[r][0]+v[r][1]+maxValue(N,v,r+1,-1,k)
            theMax = max(zero, one, negOne)
            cache[(r,unclosed, k)] = theMax
            #print (r, unclosed, k), theMax
            return theMax
        if unclosed == 0:
            zero = v[r][0]+maxValue(N,v,r+1,0,k-1)
            negOne = v[r][0]+v[r][1]+maxValue(N,v,r+1,-1,k)
            theMax = max(zero, negOne)
            cache[(r,unclosed, k)] = theMax
            #print (r, unclosed, k), theMax
            return theMax
        if unclosed == 1:
            one = v[r][1]+maxValue(N,v,r+1,1,k-1)
            negOne = v[r][0]+v[r][1]+maxValue(N,v,r+1,-1,k)
            theMax = max(one, negOne)
            cache[(r,unclosed, k)] = theMax
            #print (r, unclosed, k), theMax
            return theMax
            #return max(v[r][1]+maxValue(N,v,r+1,1,k-1), v[r][0]+v[r][1]+maxValue(N,v,r+1,-1,k))
    if k == N-r and r < N:
        if unclosed == -1:
            zero = v[r][0]+maxValue(N,v,r+1,0,k-1)
            one = v[r][1]+maxValue(N,v,r+1,1,k-1)
            theMax = max(zero, one)
            cache[(r,unclosed, k)] = theMax
            #print (r, unclosed, k), theMax
            return theMax
        if unclosed == 0:
            theMax = v[r][0]+maxValue(N,v,r+1,0,k-1)
            cache[(r,unclosed, k)] = theMax
            #print (r, unclosed, k), theMax
            return theMax
        if unclosed == 1:
            theMax = v[r][1]+maxValue(N,v,r+1,1,k-1)
            cache[(r,unclosed, k)] = theMax
            #print (r, unclosed, k), theMax
            return theMax
    return theMax

def main():
    firstLine = sys.stdin.readline().split()
    N = int(firstLine[0])
    k = int(firstLine[1])
    values = [[0 for x in range(2)] for y in range(N)]
    for i in range(0,N):
        line = sys.stdin.readline().split()
        values[i][0] = int(line[0])
        values[i][1] = int(line[1])
    sys.stdin.readline() # 0 0 last line
    count = 0
    if k <= N:
        count += maxValue(N, values, 0, -1, k);
    print count
    #for key in cache:
        #print key, cache[key]
if __name__ == "__main__":
    main()
