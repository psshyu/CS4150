import sys

def penaltyCalc(i, k):
    return (400 - (k - i)) * (400 - (k-i))

def penalty(i, stops, n, numStops, cache):
    if i+1 in cache:
        return cache[i+1]

    if i == numStops:
        return 0
    else:
        minimum = penaltyCalc(stops[i], stops[i+1])+ penalty(i+1, stops, n, numStops, cache)
        for k in range(i+2, numStops+1):
            calc = penaltyCalc(stops[i], stops[k])+ penalty(k, stops, n, numStops, cache)
            if minimum >= calc:
                minimum = calc
        cache[i+1] = minimum
        return minimum

def main():
    numStops = int(sys.stdin.readline())
    sys.stdin.readline()
    start = 0
    stops = [0]*(numStops+1)
    cache = {}
    for k in range(1, numStops+1):
       stop = int(sys.stdin.readline())
       stops[k] = stop
    lastStop = stops[len(stops)-1]
    print penalty(0, stops, lastStop, numStops, cache)

if __name__ == "__main__":
    main()