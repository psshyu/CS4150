import sys
import Queue

def main():
    students = set()
    friendships = {}
    
    numStudents = int(sys.stdin.readline())
    for i in range(0, numStudents):
        name = sys.stdin.readline().strip()
        students.add(name)
        friendships[name] = []

    numFriendships = int(sys.stdin.readline())
    for i in range(0, numFriendships):
        pair = sys.stdin.readline().strip().split()
        friendships[pair[0]].append(pair[1])
        friendships[pair[1]].append(pair[0])
    
    numRumorStarter = int(sys.stdin.readline())
    for i in range(0, numRumorStarter):
        rumorStarter = sys.stdin.readline().strip()
        heardStudents, foreverAlone = spreadRumor(students, friendships, rumorStarter)
        
        for day in heardStudents:
            day.sort()
            for i in range(0, len(day)):
                print day[i],
        if len(foreverAlone) > 0:
            for i in range(0, len(foreverAlone)-1):
                print foreverAlone[i],
            print foreverAlone[len(foreverAlone)-1]
        else:
            print("\n")

def spreadRumor(stude, friendships, spreader):
    dist = {}
    students = list(stude)
    result = [[] for i in range(len(students))]

    for student in students:
        dist[student] = sys.maxint
    dist[spreader] = 0

    result[0].append(spreader)
    students.remove(spreader)

    Q = Queue.Queue()
    Q.put(spreader)

    while Q.empty() is False:
        
        popped = Q.get()
        friends = friendships.get(popped)
                
        for friend in friends:
            if dist[friend] == sys.maxint:
                Q.put(friend)
                dist[friend] = dist[popped] + 1
                result[dist[friend]].append(friend)
                students.remove(friend)
    result = filter(None, result)
    students = filter(None, students)
    students.sort()
    return result, students

if __name__ == "__main__":
    main()