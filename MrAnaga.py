import sys

def main():
    inputLines = sys.stdin.readlines()
    strippedLines = [''.join(sorted(x.strip())) for x in inputLines] # strips the /n and alphabetizes each word
    anagramCount = {}

    for i in range(1, len(strippedLines)):
        if strippedLines[i] in anagramCount:
            anagramCount[strippedLines[i]]+= 1
        else:
            anagramCount[strippedLines[i]] = 1

    uniqueCount = 0
    for j, k in anagramCount.items():
        if 1 == k:
            uniqueCount += 1
    print uniqueCount

if __name__ == "__main__":
    main()
