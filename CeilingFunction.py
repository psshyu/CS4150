import sys

def locateIndex(currentValue, targetIndex, treeArray):
    if (treeArray[targetIndex] == 'X'):
        return targetIndex;
    elif (currentValue > treeArray[targetIndex]):
        return locateIndex(currentValue, targetIndex * 2 + 1, treeArray)
    else:
        return locateIndex(currentValue, targetIndex * 2, treeArray)

def main():
    inputLines = sys.stdin.readlines()
    strippedLines = [x.strip() for x in inputLines] # strips the /n

    prototypes = int(strippedLines[0].split()[0])
    nodes = int(strippedLines[0].split()[1]) #aka max depth
    maxTreeSize = pow(2,nodes)
    uniquePrototypes = set()

    for i in range(1, prototypes + 1):
        treeArray = ['X'] * maxTreeSize
        listOfValues = [int(i) for i in strippedLines[i].split()]
        #print listOfValues
        treeArray[1] = listOfValues[0]
        indexWithValues = set([1])
        for i in range(1, nodes):
            currentValue = listOfValues[i]
            valueIndex = locateIndex(currentValue, 1, treeArray)
            treeArray[int(valueIndex)] = currentValue
            indexWithValues.add(valueIndex)
        #print treeArray
        #print indexWithValues
        uniquePrototypes.add(frozenset(indexWithValues))

    print len(uniquePrototypes)

if __name__ == "__main__":
    main()
