import sys
import math

def calcDistance(galaxy, current, d):
    dist = math.sqrt(math.pow(int(galaxy[0])-int(current[0]),2)+math.pow(int(galaxy[1])-int(current[1]),2))
    if dist <= d:
        return True
    else:
        return False

def main():
    inputLines = sys.stdin.readlines()
    strippedLines = [x.strip() for x in inputLines] # strips the /n
    firstLine = strippedLines[0].split()

    d = int(firstLine[0])
    k = int(firstLine[1])

    majCount = 0
    candidate = (0,0)

    for i in range(1, k+1):
        coords = strippedLines[i].split()
        if majCount == 0:
            candidate = (int(coords[0]), int(coords[1]))
        if calcDistance(candidate, coords, d):
            majCount += 1
        else:
            majCount -= 1

    majCount = 0
    for i in range(1, k+1):
        coords = strippedLines[i].split()
        if calcDistance(candidate, coords, d):
            majCount += 1

    if majCount > k/2:
        print majCount
    else:
        print "NO"


if __name__ == "__main__":
    main()
