import sys
import heapq
import Queue

def main():
    firstLine = sys.stdin.readline().split()
    N = int(firstLine[0])
    T = int(firstLine[1])
    values  = [0]*T
    heap = []
    timeAndMoney = {}
    
    #heapq.heapify(heap)
    for i in range(0,N):
        line = sys.stdin.readline().split()
        c = int(line[0])
        t = int(line[1])
        
        if t not in timeAndMoney:
            timeAndMoney[t] = Queue.Queue()

        timeAndMoney[t].put(-1*c)

    #print "Dict Length: " + str(len(timeAndMoney))
    #for x in timeAndMoney:
        #print x
        #print list(timeAndMoney[x].queue)

    total = 0
    for i in range(T, -1, -1):
        #print i
        if i in timeAndMoney:
            while timeAndMoney[i].empty() is False:
                heapq.heappush(heap, timeAndMoney[i].get())
                #print heap
            total += heapq.heappop(heap)
    print abs(total)    
if __name__ == "__main__":
    main()
